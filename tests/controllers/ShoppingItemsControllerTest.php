<?php

use App\ShoppingItem;
use App\ShoppingList;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ShoppingItemsControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $listsMock;
    protected $itemsMock;
    protected $testFields = array(
        'name',
        'description',
        'completed',
        'slug'
    );

    private $_validData = array(
        [
            'name'  => 'Generic Shopping Item',
            'description'   => 'This is a description',
            'completed' => true
        ],
        [
            'name'  => 'Shopping Item-1',
            'completed' => 0
        ]
    );

    private $_invalidData = array(
        [
            'name'  => ''
        ],
        [
            'description'   => ''
        ]
    );

    public function setup()
    {
        parent::setup();
        $this->listsMock = Mockery::mock('App\ShoppingList');
        $this->itemsMock = Mockery::mock('App\ShoppingItem');
    }

    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

    public function testItemIndexGet()
    {
        $numberOfItems = 2;
        $mockList = factory(ShoppingList::class)
                    ->create();
        for ($i=0; $i<$numberOfItems; $i++)
        {
            $mockList->items()->save(
                factory(ShoppingItem::class)->make()
            );
        }

        $items = $mockList->items;

        $response = $this->call('get', '/lists/' . $mockList->slug. '/items');

        // test for ok response
        $this->assertTrue($response->isOk());
        $responseItems = json_decode($response->getContent());

        // test for required attributes to response
        $this->assertObjectHasAttribute("items", $responseItems);
        $this->assertCount($numberOfItems, $responseItems->items);

        // test that each object in array has correct format
        foreach($responseItems->items as $item)
        {
            foreach($this->testFields as $field)
            {
                $this->assertObjectHasAttribute($field, $item);
            }
        }

        // test for non-existent list response
        $falseResponse = $this->call('get', '/lists/' . substr($mockList->slug, 0, -1). '/items');
        $this->assertTrue($falseResponse->isNotFound());
    }

    public function testItemShowGet()
    {
        $mockList = factory(ShoppingList::class)->create();
        $mockItem = factory(ShoppingItem::class)->create();
        $mockList->items()->save($mockItem);
        $this->itemsMock
                ->shouldReceive('find')
                ->andReturn($mockItem);
        $response = $this->call('get', '/lists/' . $mockList->slug . '/items/' . $mockItem->slug);

        // test for ok response
        $this->assertTrue($response->isOk());
        $item = json_decode($response->getContent());
        $this->assertNotNull($item);

        // test for item object attributes
        foreach($this->testFields as $field)
        {
            $this->assertObjectHasAttribute($field, $item);
        }

        // test for non-existent list
        $falseResponse = $this->call('get', '/lists/' . $mockList->slug. '/items/' . substr($mockItem->slug, 0, -1));
        $this->assertTrue($falseResponse->isNotFound());
    }

    public function testItemUpdatePut()
    {
        $mockList = factory(ShoppingList::class)->create();
        $mockItem = factory(ShoppingItem::class)->create();

        // test for non-existent list
        $emptyResponse = $this->call('put', '/lists/' . substr($mockList->slug, 0, -1) . '/items/' . $mockItem->slug);
        $this->assertTrue($emptyResponse->isNotFound());

        // test for non-existent item
        $emptyResponse = $this->call('put', '/lists/' . $mockList->slug. '/items/' . substr($mockItem->slug, 0, -1));
        $this->listsMock
                ->shouldReceive('find')
                ->andReturn($mockList);
        $this->assertTrue($emptyResponse->isNotFound());

        // test no data
        $this->listsMock
            ->shouldReceive('find')
            ->andReturn($mockList);
        $this->itemsMock
            ->shouldReceive('find')
            ->andReturn($mockItem);
        $response = $this->call('put', '/lists/' . $mockList->slug . '/items/' . $mockItem->slug);
        $this->assertTrue($response->getStatusCode() == 400);

        // test invalid data
        foreach($this->_invalidData as $invalid)
        {
            $response = $this->call('put', '/lists/' . $mockList->slug . '/items/' . $mockItem->slug, $invalid);
            $this->assertTrue($response->getStatusCode() == 400);
            $this->assertObjectHasAttribute('errors', json_decode($response->getContent()));
        }

        // test valid data
        foreach($this->_validData as $valid)
        {
            $testItem = factory(ShoppingItem::class)->create();

            $this->itemsMock
                    ->shouldReceive('save')
                    ->andReturn($testItem);

            // test status code
            $response = $this->call('put', '/lists/' . $mockList->slug . '/items/' . $mockItem->slug, $valid);
            $this->assertTrue($response->getStatusCode() == 200);

            // test json content
            $item = json_decode($response->getContent());
            foreach($this->testFields as $field)
            {
                $this->assertObjectHasAttribute($field, $item);
            }
        }
    }

    public function testItemStorePost()
    {
        $mockList = factory(ShoppingList::class)->create();

        // test for non-existent list
        $emptyResponse = $this->call('post', '/lists/' . substr($mockList->slug, 0, -1) . '/items/');
        $this->assertTrue($emptyResponse->isNotFound());

        // test no data
        $this->listsMock
            ->shouldReceive('find')
            ->andReturn($mockList);
        $response = $this->call('post', '/lists/' . $mockList->slug . '/items/');
        $this->assertTrue($response->getStatusCode() == 400);

        // test invalid data
        foreach($this->_invalidData as $invalid)
        {
            $response = $this->call('post', '/lists/' . $mockList->slug . '/items/', $invalid);
            $this->assertTrue($response->getStatusCode() == 400);
            $this->assertObjectHasAttribute('errors', json_decode($response->getContent()));
        }

        // test valid data
        foreach($this->_validData as $valid)
        {
            $testItem = factory(ShoppingItem::class)->create();

            $this->itemsMock
                ->shouldReceive('save')
                ->andReturn($testItem);

            // test status code
            $response = $this->call('post', '/lists/' . $mockList->slug . '/items/', $valid);
            $this->assertTrue($response->getStatusCode() == 201);

            // test json content
            $item = json_decode($response->getContent());
            foreach($this->testFields as $field)
            {
                $this->assertObjectHasAttribute($field, $item);
            }
        }
    }

    public function testItemDelete()
    {
        $mockList = factory(ShoppingList::class)->create();
        $this->listsMock
                ->shouldReceive('find')
                ->andReturn($mockList);
        $mockItem = factory(ShoppingItem::class)->create();
        $this->itemsMock
                ->shouldReceive('find')
                ->andReturn($mockItem);
        $this->itemsMock
                ->shouldReceive('delete')
                ->andReturn(true);
        $response = $this->call('delete', '/lists/' . $mockList->slug . '/items/' . $mockItem->slug);

        // test for proper response
        $this->assertTrue($response->getStatusCode() == 204);

        // test for non-existent list
        $falseResponse = $this->call('delete', '/lists/' . substr($mockList->slug, 0, -1) . '/items' . $mockItem->slug);
        $this->assertTrue($falseResponse->isNotFound());

        // test for non-existent item
        $falseResponse = $this->call('delete', '/lists/' . $mockList->slug . '/items' . substr($mockItem->slug, 0, -1));
        $this->assertTrue($falseResponse->isNotFound());
    }
}
