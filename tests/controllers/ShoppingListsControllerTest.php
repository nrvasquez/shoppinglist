<?php

use App\ShoppingList;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ShoppingListsControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $listsMock;
    protected $testFields = array(
        'id',
        'name',
        'url',
        'description',
        'slug'
    );

    private $_validData = array(
        [
            'name'  => 'Generic Shopping List',
            'description'   => 'Generic Description'
        ],
        [
            'name'  => 'This is a Shopping List with no Description'
        ]
    );
    private $_invalidData = array(
        [
            'name'  => ''
        ]
    );

    public function setup()
    {
        parent::setup();
        $this->listsMock = Mockery::mock('App\ShoppingLists');
    }

    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

    public function testListIndexGet()
    {
        $numberOfLists = 2;
        $this->listsMock
                ->shouldReceive('all')
                ->andReturn(factory(ShoppingList::class, $numberOfLists)
                    ->create());
        $response = $this->call('get', '/lists');

        // test for ok response
        $this->assertTrue($response->isOk());
        $lists = json_decode($response->getContent());

        // test for required attributes to response
        $this->assertObjectHasAttribute("lists", $lists);
        $this->assertCount($numberOfLists, $lists->lists);

        // test that each object in array has correct format

        foreach($lists->lists as $list)
        {
            foreach($this->testFields as $field)
            {
                $this->assertObjectHasAttribute($field, $list);
            }
        }
    }

    public function testListShowGet()
    {
        $mockList = factory(ShoppingList::class)->create();
        $this->listsMock
                ->shouldReceive('find')
                ->andReturn($mockList);
        $response = $this->call('get', '/lists/' . $mockList->slug);

        // test for ok response
        $this->assertTrue($response->isOk());
        $list = json_decode($response->getContent());
        $this->assertNotNull($list);

        // test for object attributes
        foreach($this->testFields as $field)
        {
            $this->assertObjectHasAttribute($field, $list);
        }

        // test for non-existent list
        $falseResponse = $this->call('get', '/lists/' . substr($mockList->slug, 0, -1));

        $this->assertTrue($falseResponse->isNotFound());

    }

    public function testListUpdatePut()
    {
        $mockList = factory(ShoppingList::class)->create();

        // test for non-existent list
        $emptyResponse = $this->call('put', '/lists/' . substr($mockList->slug, 0, -1));
        $this->assertTrue($emptyResponse->isNotFound());

        $this->listsMock
            ->shouldReceive('find')
            ->andReturn($mockList);

        // test no data
        $response = $this->call('put', '/lists/' . $mockList->slug);
        $this->assertTrue($response->getStatusCode() == 400);

        // test invalid data
        foreach($this->_invalidData as $invalid)
        {
            $response = $this->call('put', '/lists/' . $mockList->slug, $invalid);
            $this->assertTrue($response->getStatusCode() == 400);
            $this->assertObjectHasAttribute('errors', json_decode($response->getContent()));
        }

        // test valid data
        foreach($this->_validData as $valid)
        {
            $testList = factory(ShoppingList::class)->create();

            $this->listsMock
                ->shouldReceive('save')
                ->andReturn($testList);

            // test status code
            $response = $this->call('put', '/lists/' . $mockList->slug, $valid);
            $this->assertTrue($response->getStatusCode() == 200);

            // test json content
            $list = json_decode($response->getContent());
            foreach($this->testFields as $field)
            {
                $this->assertObjectHasAttribute($field, $list);
            }
        }
    }

    public function testListStorePost()
    {
        // test no data
        $response = $this->call('post', '/lists');
        $this->assertTrue($response->getStatusCode() == 400);

        // test invalid data
        foreach($this->_invalidData as $invalid)
        {
            $response = $this->call('post', '/lists', $invalid);
            $this->assertTrue($response->getStatusCode() == 400);
            $this->assertObjectHasAttribute('errors', json_decode($response->getContent()));
        }

        // test valid data
        foreach($this->_validData as $valid)
        {
            $testList = factory(ShoppingList::class)->create();

            $this->listsMock
                ->shouldReceive('save')
//                ->once()
                ->andReturn($testList);

            // test status code
            $response = $this->call('post', '/lists', $valid);
            $this->assertTrue($response->getStatusCode() == 201);

            // test json content
            $list = json_decode($response->getContent());
            foreach($this->testFields as $field)
            {
                $this->assertObjectHasAttribute($field, $list);
            }
        }
    }

    public function testListDelete()
    {
        $mockList = factory(ShoppingList::class)->create();
        $this->listsMock
                ->shouldReceive('find')
                ->andReturn($mockList);
        $this->listsMock
                ->shouldReceive('delete')
                ->andReturn(true);
        $response = $this->call('delete', '/lists/' . $mockList->slug);

        // test for proper response
        $this->assertTrue($response->getStatusCode() == 204);

        // test for non-existent list
        $falseResponse = $this->call('delete', '/lists/' . substr($mockList->slug, 0, -1));
        $this->assertTrue($falseResponse->isNotFound());
    }
}
