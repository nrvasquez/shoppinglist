<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ShoppingItemTest extends TestCase
{
    /**
     *
     * @return void
     */
    public function testRelationWithList()
    {
        factory(\App\ShoppingList::class, 2)
            ->create()
            ->each(function($l) {
                $l->items()->save(factory(\App\ShoppingItem::class)->make());
            });
        $item = \App\ShoppingItem::first();
        $this->assertEquals($item->shoppingList->id, $item->list_id);
    }
}
