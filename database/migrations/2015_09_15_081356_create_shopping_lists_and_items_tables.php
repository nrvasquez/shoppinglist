<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingListsAndItemsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('');
            $table->string('slug')->default('');
            $table->text('description')->default('');
            $table->timestamps();
        });

        Schema::create('shopping_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('list_id')->unsigned()->default(0);
            $table->foreign('list_id')->references('id')->on('shopping_lists')->onDelete('cascade');
            $table->string('name')->default('');
            $table->string('slug')->default('');
            $table->text('description')->default('');
            $table->boolean('completed')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("shopping_items");
        Schema::drop("shopping_lists");
    }
}
