<?php

use Illuminate\Database\Seeder;

class ShoppingListsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('shopping_lists')->delete();

        $lists = array(
            [
                'id'    => 1,
                'name'  => 'List 1',
                'slug'  => 'list-1',
                'description'   => "This is the first list",
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'id'    => 2,
                'name'  => 'List 2',
                'slug'  => 'list-2',
                'description'   => "This is the second list",
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'id'    => 3,
                'name'  => 'Test List',
                'slug'  => 'test-list',
                'description'   => "This is the test list",
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ]
        );

        DB::table('shopping_lists')->insert($lists);
    }

}