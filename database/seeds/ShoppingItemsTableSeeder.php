<?php

use Illuminate\Database\Seeder;

class ShoppingItemsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('shopping_items')->delete();

        $items = array(
            [
                'id'    => 1,
                'list_id'   => 1,
                'name'  => 'Item 1',
                'slug'  => 'item-1',
                'description'   => "This is the first item",
                'completed'     => false,
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'id'    => 2,
                'list_id'   => 1,
                'name'  => 'Item 2',
                'slug'  => 'item-2',
                'description'   => "This is the second item",
                'completed'     => false,
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'id'    => 3,
                'list_id'   => 1,
                'name'  => 'Item 3',
                'slug'  => 'item-3',
                'description'   => "This is the third item",
                'completed'     => true,
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'id'    => 4,
                'list_id'   => 2,
                'name'  => 'Item 4',
                'slug'  => 'item-4',
                'description'   => "This is the first item in the second list",
                'completed'     => false,
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'id'    => 5,
                'list_id'   => 2,
                'name'  => 'Item 5',
                'slug'  => 'item-5',
                'description'   => "This is the fifth item",
                'completed'     => true,
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'id'    => 6,
                'list_id'   => 3,
                'name'  => 'Test Item 1',
                'slug'  => 'test-item-1',
                'description'   => "This is the first test item",
                'completed'     => false,
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ],
            [
                'id'    => 7,
                'list_id'   => 3,
                'name'  => 'Test Item 2',
                'slug'  => 'test-item-1',
                'description'   => "This is the second test item",
                'completed'     => true,
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ]
        );

        DB::table('shopping_items')->insert($items);
    }

}