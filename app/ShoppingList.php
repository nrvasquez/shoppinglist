<?php

namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class ShoppingList extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from'    => 'name'
    ];

    public function items()
    {
        return $this->hasMany('App\ShoppingItem', 'list_id', 'id');
    }
}
