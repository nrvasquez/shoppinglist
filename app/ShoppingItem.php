<?php

namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class ShoppingItem extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from'    => 'name'
    ];

    public function shoppingList()
    {
        return $this->belongsTo('App\ShoppingList', 'list_id');
    }
}
