<?php

namespace App\Http\Controllers;

use App\ShoppingList;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Response;

class ShoppingListsController extends Controller
{
    protected $rules =  [
        'name'  => ['required', 'max:255'],
    ];

    private function _listObjectResponse(ShoppingList $list)
    {
        return array(
            'id'    => $list->id,
            'name'  => $list->name,
            'description'   => $list->description,
            'slug'  => $list->slug,
            'url'   => route('lists.show', $list->slug)
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $response = [
            'lists' => []
        ];
        $lists = ShoppingList::all();
        foreach($lists as $list)
        {
            $response['lists'][] = $this->_listObjectResponse($list);
        }
        return Response::json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $statusCode = 400;
        $response = array();

        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails())
        {
            $response = array(
                'message'   => 'Invalid data format!',
                'errors'    => $validator->errors()
            );
        }
        else
        {
            $list = new ShoppingList();
            $list->name = $request->name;
            if ($request->description)
            {
                $list->description = $request->description;
            }
            if ($list->save())
            {
                $statusCode = 201;
                $response = $this->_listObjectResponse($list);
            }
            else
            {
                $statusCode = 500;
                $response = [
                    'message'   => 'List not saved. Please contact the administrator.'
                ];
            }
        }

        return Response::json($response, $statusCode);
    }

    /**
     * Display the specified resource.
     *
     * @param ShoppingList $list
     * @return Response
     * @internal param int $id
     */
    public function show(ShoppingList $list = null)
    {
        $statusCode = 200;

        if ($list->id != null)
        {
            $response = $this->_listObjectResponse($list);
        }
        else
        {
            $statusCode = 404;
            $response = array(
                'message'   => 'List not found!'
            );
        }
        return Response::json($response,$statusCode);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param ShoppingList $list
     * @return Response
     * @internal param int $id
     */
    public function update(Request $request, ShoppingList $list)
    {
        $statusCode = 204;

        if ($list->id != null)
        {
            $validator = Validator::make($request->all(), $this->rules);

            if ($validator->fails())
            {
                $statusCode = 400;
                $response = array(
                    'message'   => 'Invalid data format',
                    'errors'    => $validator->errors()
                );
            }
            else
            {
                $list->name = $request->name;
                if ($request->description)
                {
                    $list->description = $request->description;
                }
                if ($list->save())
                {
                    $statusCode = 200;
                    $response = $this->_listObjectResponse($list);
                }
                else
                {
                    $statusCode = 500;
                    $response = [
                        'message'   => 'List not saved. Please contact the administrator.'
                    ];
                }
            }
        }
        else
        {
            $statusCode = 404;
            $response = array(
                'message'   => 'List not found!'
            );
        }

        return Response::json($response, $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ShoppingList $list
     * @return Response
     * @throws \Exception
     * @internal param int $id
     */
    public function destroy(ShoppingList $list)
    {
        $statusCode = 204;

        if ($list->id != null)
        {
            if ($list->delete())
            {
                $response = array(
                    'message'   => 'List successfully deleted!'
                );
            }
            else
            {
                $statusCode = 500;
                $response = array(
                    'message'   => 'Something went wrong. Please contact the administrator.'
                );
            }
        }
        else
        {
            $statusCode = 404;
            $response = array(
                'message'   => 'List not found!'
            );
        }
        return Response::json($response, $statusCode);
    }
}
