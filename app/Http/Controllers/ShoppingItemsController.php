<?php

namespace App\Http\Controllers;

use App\ShoppingItem;
use App\ShoppingList;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Illuminate\Support\Facades\Validator;

/**
 * Class ShoppingItemsController
 * @package App\Http\Controllers
 */
class ShoppingItemsController extends Controller
{
    protected $rules = [
        'name'  => ['required', 'max:255'],
        'completed' => ['required', 'boolean']
    ];

    private function _itemObjectResponse(ShoppingItem $item)
    {
        //TODO: resolve error in model relationship for getting item shoppingList
        @$listSlug = $item->shoppingList->slug;

        return array(
            'id'    => $item->id,
            'name'  => $item->name,
            'description'   => $item->description,
            'completed' => $item->completed,
            'slug'  => $item->slug,
            'list'  => $listSlug,
            'url'   => route('lists.items.show', [$listSlug, $item->slug])
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(ShoppingList $list)
    {
        $statusCode = 200;
        if ($list->id != null)
        {
            $response = array(
                'items' => []
            );
            $items = $list->items;
            foreach($list->items as $item)
            {
                $response['items'][] = $this->_itemObjectResponse($item);
            }
        }
        else
        {
            $statusCode = 404;
            $response = array(
                'message'   => 'List not found!'
            );
        }

        return Response::json($response, $statusCode);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request, ShoppingList $list)
    {
        $statusCode = 400;
        $response = array();

        if ($list->id != null)
        {
            $validator = Validator::make($request->all(), $this->rules);

            if ($validator->fails())
            {
                $response = array(
                    'message'   => 'Invalid data format!',
                    'errors'    => $validator->errors()
                );
            }
            else
            {
                $item = new ShoppingItem();
                $item->name = $request->name;
                if ($request->description)
                {
                    $item->description = $request->description;
                }
                $item->completed = $request->completed;
                if ($list->items()->save($item))
                {
                    $statusCode = 201;
                    $response = $this->_itemObjectResponse($item);
                }
                else
                {
                    $statusCode = 500;
                    $response = array(
                        'message'   => 'Item not saved. Please contact the administrator.'
                    );
                }
            }
        }
        else
        {
            $statusCode = 404;
            $response = array(
                'message'   => 'Shopping list not found!'
            );
        }

        return Response::json($response, $statusCode);
    }

    /**
     * Display the specified resource.
     *
     * @param ShoppingList $list
     * @param ShoppingItem $item
     * @return Response
     * @internal param int $id
     */
    public function show(ShoppingList $list = null, ShoppingItem $item = null)
    {
        $statusCode = 200;

        if ($item->id != null && $list->id != null)
        {
            $response = $this->_itemObjectResponse($item);
        }
        else
        {
            $statusCode = 404;
            $response = array(
                'message'   => 'Item not found!'
            );
        }
        return Response::json($response, $statusCode);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param ShoppingList $list
     * @param ShoppingItem $item
     * @return Response
     * @internal param int $id
     */
    public function update(Request $request, ShoppingList $list = null, ShoppingItem $item = null)
    {
        $statusCode = 204;
        $response = array();

        if ($list->id != null && $item->id != null)
        {
            $validator = Validator::make($request->all(), $this->rules);

            if ($validator->fails())
            {
                $statusCode = 400;
                $response = array(
                    'message'   => 'Invalid data format',
                    'errors'    => $validator->errors()
                );
            }
            else
            {
                $item->name = $request->name;
                if ($request->description)
                {
                    $item->description = $request->description;
                }
                $item->completed = $request->completed;
                if ($item->save())
                {
                    $statusCode = 200;
                    $response = $this->_itemObjectResponse($item);
                }
                else
                {
                    $statusCode = 500;
                    $response = [
                        'message'   => 'Item not saved. Please contact the administrator.'
                    ];
                }
            }
        }
        else
        {
            $statusCode = 404;
            $response = array(
                'message'   => 'Item not found!'
            );
        }
        return Response::json($response, $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ShoppingList $list
     * @param ShoppingItem $item
     * @return Response
     * @internal param int $id
     */
    public function destroy(ShoppingList $list, ShoppingItem $item)
    {
        $statusCode = 204;
        if ($list->id != null && $item->id != null)
        {
            if ($item->delete())
            {
                $response = array(
                    'message'   => 'Item successfully deleted'
                );
            }
            else
            {
                $statusCode = 500;
                $response = array(
                    'message'   => 'Something went wrong. Please contact the administrator.'
                );
            }
        }
        else
        {
            $statusCode = 404;
            $response = array(
                'message'   => 'Item not found!'
            );
        }

        return Response::json($response, $statusCode);
    }
}
