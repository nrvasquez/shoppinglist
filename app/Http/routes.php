<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::model('items', 'ShoppingItem');
Route::model('lists', 'ShoppingList');

Route::bind('lists', function($value, $route){
    return App\ShoppingList::whereSlug($value)->first();
});
Route::bind('items', function($value, $route){
    return \App\ShoppingItem::whereSlug($value)->first();
});

Route::resource('lists.items', 'ShoppingItemsController',
    ['except' => ['create', 'edit']]);
Route::resource('lists', 'ShoppingListsController',
    ['except' => ['create', 'edit']]);

Route::post('auth/facebook', 'AuthController@facebook');
Route::get('auth/me', ['middleware' => 'auth', 'uses' => 'UserController@getUser']);